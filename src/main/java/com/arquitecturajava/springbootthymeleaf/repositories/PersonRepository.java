package com.arquitecturajava.springbootthymeleaf.repositories;

import com.arquitecturajava.springbootthymeleaf.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person,String> {

}
