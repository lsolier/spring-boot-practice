package com.arquitecturajava.springbootthymeleaf;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="people")
public class Person {

  @Id
  @Column(name="name")
  private String name;

  private String lastname;
  private Integer age;

  public Person(String name){
    setName(name);
  }

}
