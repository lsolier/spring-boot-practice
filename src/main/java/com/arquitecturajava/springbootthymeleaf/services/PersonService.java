package com.arquitecturajava.springbootthymeleaf.services;

import com.arquitecturajava.springbootthymeleaf.Person;

public interface PersonService {

  Iterable<Person> findAll();

  void insert(Person person);

  void delete(String name);

}
