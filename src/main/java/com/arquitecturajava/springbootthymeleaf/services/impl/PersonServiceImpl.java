package com.arquitecturajava.springbootthymeleaf.services.impl;

import com.arquitecturajava.springbootthymeleaf.Person;
import com.arquitecturajava.springbootthymeleaf.repositories.PersonRepository;
import com.arquitecturajava.springbootthymeleaf.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {

  @Autowired
  PersonRepository personRepository;

  public Iterable<Person> findAll(){

    return personRepository.findAll();

  }

  public void insert(Person person){

    personRepository.save(person);

  }

  public void delete(String name){
    Optional<Person> person = personRepository.findById(name);
    if (person.isPresent()){
      personRepository.delete(person.get());
    }

  }
}
