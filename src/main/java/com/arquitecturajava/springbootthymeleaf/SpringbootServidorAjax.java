package com.arquitecturajava.springbootthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootServidorAjax {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServidorAjax.class, args);
	}

}
