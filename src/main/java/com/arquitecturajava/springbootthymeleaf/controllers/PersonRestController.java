package com.arquitecturajava.springbootthymeleaf.controllers;

import com.arquitecturajava.springbootthymeleaf.Person;
import com.arquitecturajava.springbootthymeleaf.services.PersonService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/webapi")
@CrossOrigin(origins = "http://localhost:9001",methods = {RequestMethod.GET,RequestMethod.POST})
public class PersonRestController {

  @Autowired
  private PersonService personService;

  public PersonRestController(PersonService personService){
      this.personService = personService;
  }

  @GetMapping("/people")
  public Iterable<Person> showPeople(){
    return personService.findAll();
  }

  @PostMapping(value = "/people",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public void insert(@RequestBody Person person){
    personService.insert(person);
  }

  @DeleteMapping("/people/{name}")
  public void delete(@PathVariable String name){
    personService.delete(name);
  }

}
